from lib.linear_system_helper import *
from typing import *
from decimal import *
getcontext().prec = 100


def cos_dec(x):
    getcontext().prec += 2
    i, lasts, s, fact, num, sign = 0, 0, 1, 1, 1, 1
    while s != lasts:
        lasts = s
        i += 2
        fact *= i * (i-1)
        num *= x * x
        sign *= -1
        s += num / fact * sign
    getcontext().prec -= 2
    return +s


def solve_and_print_eq(a, f_col):
    task = LinearEquationWith1Sol(a, f_col)
    print("A=")
    task.print()
    print("F=")
    task.f.print()
    print("Решение СЛАУ:")
    print("x=")
    task.solve_gauss_classic().print()
    print("x= (методом выбора главного элемента)")
    task.solve_gauss_main_element().print()


print("Практикум по численным методам",
      "Задание 1",
      "Выполнил студент 206 группы Булатов В.А.",
      "",
      "Решение СЛАУ методом Гаусса с выбором главного элемента",
      "СЛАУ представлены в виде Ax=F, где",
      "A - матрица коэффициентов",
      "x - вектор-столбец неизвестных переменных",
      "F - вектор-столбец свободных членов",
      "",
      "Приложение 1, вариант 7",
      sep='\n')

print("СЛАУ 1: ")
task_1_a = [
    [2, -1, -6, 3],
    [7, -4, 2, -15],
    [1, -2, -4, 9],
    [1, -1, 2, -6]
]
task_1_col = [
    [-1],
    [-32],
    [5],
    [-8]
]

solve_and_print_eq(task_1_a, task_1_col)
print("СЛАУ 2: ")
task_2_a = [
    [1, -2, 1, 1],
    [2, 1, -1, -1],
    [3, -1, -2, 1],
    [5, 2, -1, 9]
]
task_2_col = [
    [0],
    [0],
    [0],
    [-10]
]
solve_and_print_eq(task_2_a, task_2_col)
(Matrix(task_2_a)*Matrix(task_2_a).inv()).print()

print("СЛАУ 3: ")
task_3_a = [
    [8, 6, 5, 2],
    [3, 3, 2, 1],
    [4, 2, 3, 1],
    [7, 4, 5, 2]
]
task_3_col = [
    [21],
    [10],
    [8],
    [18]
]
solve_and_print_eq(task_3_a, task_3_col)


print("Приложение 2, вариант 2-6")

M = Decimal(10)
n = 100
qM = Decimal(1.001) - Decimal(2) * M * Decimal(0.001)

task_a = [[(qM ** Decimal(i+j) + Decimal(0.1)*Decimal(j-i)) if i != j else (Decimal(qM-Decimal(1)) ** Decimal(i+j))
           for j in range(1, n+1)] for i in range(1, n+1)]
task_col = [[Decimal(n)*((Decimal(n)/Decimal(i)).exp())*cos_dec(Decimal(n)/Decimal(i))] for i in range(1, n+1)]

solve_and_print_eq(task_a, task_col)
