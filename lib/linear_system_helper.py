from __future__ import annotations
from typing import *
from copy import deepcopy

EPS = float('1E-8')

CNT = 0

def dec_zero(dec: float) -> bool:
    return abs(dec) < EPS


def dec_one(dec: float) -> bool:
    return abs(dec-float(1)) < EPS


class Matrix:
    def __init__(self,
                 matrix_data: Union[List[List[Union[
                     float,
                     int,
                     float
                 ]]], Matrix]) -> NoReturn:
        if isinstance(matrix_data, Matrix):
            self.matrix = deepcopy(matrix_data.matrix)
        else:
            self.matrix = deepcopy(matrix_data)
        self.dim_rows = len(self.matrix)
        assert self.dim_rows != 0, "Row count can't be zero"
        self.dim_cols = len(self[0])
        assert self.dim_cols != 0, "Column count can't be zero"
        for i in range(self.dim_rows):
            assert len(self[i]) == self.dim_cols, "Input matrix is not rectangular"
        for i in range(self.dim_rows):
            for j in range(self.dim_cols):
                self[i][j] = float(self[i][j])

    def __getitem__(self, row: int) -> List[float]:
        return self.matrix[row]

    def __setitem__(self, row: int, val) -> NoReturn:
        self.matrix[row] = val
    
    def __copy__(self) -> Matrix:
        return Matrix(self.matrix)

    def copy(self) -> Matrix:
        return self.__copy__()

    def print(self, dec_places: int = 5) -> NoReturn:
        for i in range(self.dim_rows):
            first = i == 0
            last = (i + 1) == self.dim_rows
            if first and last:
                print('( ', end='')
            elif first:
                print('/ ', end='')
            elif last:
                print('\\ ', end='')
            else:
                print('| ', end='')
            for item in self[i]:
                print(round(item, dec_places), end=' ')
            if first and last:
                print(' )')
            elif first:
                print(' \\')
            elif last:
                print(' /')
            else:
                print(' |')

    def tex(self, dec_places: int = 2, max_row_cnt: int = 10, max_col_cnt: int = 10) -> str:
        ans = '\\left[\\begin{tabular}{ '
        for i in range(min(self.dim_cols, max_col_cnt + 1)):
            ans += 'c '
        ans += '}\n'
        for i in range(min(self.dim_rows, max_row_cnt + 1)):
            if (self.dim_rows <= max_row_cnt) or (i < max_row_cnt):
                ans += ' & '.join([str(round(self.matrix[i][j], dec_places))
                                   if (self.dim_cols <= max_col_cnt) or (j < max_col_cnt) else
                                   ('\\cdots' if i == min(self.dim_rows, max_row_cnt + 1)//2 else '')
                                   for j in range(min(self.dim_cols, max_col_cnt + 1))])
                if self.dim_cols > max_col_cnt:
                    ans += ' & ' + str(round(self.matrix[i][-1], dec_places))
            elif i == max_row_cnt:
                ans += ' & '.join(['\\vdots' if j == min(self.dim_cols, max_col_cnt + 1)//2 else ('\\ddots' if i == j else '')
                                   for j in range(min(self.dim_cols, max_col_cnt + 1))])
                ans += ' \\\\\n'
                ans += ' & '.join(
                    [str(round(self.matrix[-1][j], dec_places))
                     if (self.dim_cols <= max_col_cnt) or (j < max_col_cnt) else
                     ('\\cdots' if i == min(self.dim_rows, max_row_cnt + 1) // 2 else '')
                     for j in range(min(self.dim_cols, max_col_cnt + 1))])
            if i != min(self.dim_rows, max_row_cnt + 1) - 1:
                ans += ' \\\\\n'
        ans += '\n\\end{tabular}\\right]'
        return ans

    def __mul__(self, other: Union[Matrix, int, float, float]) -> Matrix:
        if type(other) == Matrix:
            assert self.dim_cols == other.dim_rows, f"Can't multiply matrices with dimensions: " \
                                                    "{self.dimRows}x{self.dimCols}, {other.dimRows}x{other.dimCols}"
            return func_matrix(self.dim_rows,
                               other.dim_cols,
                               (lambda row, col, dim_r, dim_c:
                                sum((self[row-1][i] * other[i][col-1] for i in range(self.dim_cols)))))
        else:
            mat_copy = self.copy()
            for i in range(self.dim_rows):
                mat_copy.row_scale(i, other)
            return mat_copy

    def __add__(self, other: Matrix):
        assert self.dim_cols == other.dim_cols and self.dim_rows == other.dim_rows, \
                                                "Can't multiply matrices with dimensions: " \
                                               f"{self.dim_rows}x{self.dim_cols}, {other.dim_rows}x{other.dim_cols}"
        return func_matrix(dim_rows=self.dim_rows,
                           dim_cols=self.dim_cols,
                           func=lambda i, j, n1, n2: self[i-1][j-1] + other[i-1][j-1])

    def __sub__(self, other: Matrix):
        assert self.dim_cols == other.dim_cols and self.dim_rows == other.dim_rows, \
                                                "Can't multiply matrices with dimensions: " \
                                               f"{self.dim_rows}x{self.dim_cols}, {other.dim_rows}x{other.dim_cols}"
        return func_matrix(dim_rows=self.dim_rows,
                           dim_cols=self.dim_cols,
                           func=lambda i, j, n1, n2: self[i-1][j-1] - other[i-1][j-1])

    def transposed(self) -> Matrix:
        return func_matrix(self.dim_cols,
                           self.dim_rows,
                           (lambda row, col, dim_r, dim_c: self[col-1][row-1]))

    def swap_cols(self, col1: int, col2: int) -> NoReturn:
        assert col1 < self.dim_cols and col2 < self.dim_cols, 'Column out of range'
        if col1 != col2:
            for i in range(self.dim_rows):
                self.matrix[i][col1], self.matrix[i][col2] = self.matrix[i][col2], self.matrix[i][col1]

    def swap_rows(self, row1: int, row2: int) -> NoReturn:
        assert row1 < self.dim_rows and row2 < self.dim_rows, 'Row out of range'
        if row1 != row2:
            self.matrix[row1], self.matrix[row2] = self.matrix[row2], self.matrix[row1]

    def row_scale(self,
                  row: int,
                  scale: Union[int, float, float]) -> NoReturn:
        assert row < self.dim_rows, 'Row out of range'
        for i in range(self.dim_cols):
            self.matrix[row][i] *= float(scale)

    def col_scale(self,
                  col: int,
                  scale: Union[int, float, float]) -> NoReturn:
        assert col < self.dim_cols, 'Column out of range'
        for i in range(self.dim_rows):
            self.matrix[i][col] *= float(scale)

    def row_linear_operation(self,
                             row1: int,
                             row2: int,
                             scale: Union[int, float, float]) -> NoReturn:
        assert row1 < self.dim_rows and row2 < self.dim_rows, 'Row out of range'
        assert row1 != row2, 'Same row'
        for i in range(self.dim_cols):
            self.matrix[row2][i] += float(scale) * self.matrix[row1][i]

    def column_linear_operation(self,
                                col1: int,
                                col2: int,
                                scale: Union[int, float, float]) -> NoReturn:
        assert self.dim_rows == self.dim_cols, "Non-square matrix"
        assert col1 < self.dim_cols and col2 < self.dim_cols, 'Column out of range'
        assert col1 != col2, 'Same column'
        for i in range(self.dim_rows):
            self.matrix[i][col2] += float(scale) * self.matrix[i][col1]

    def det(self) -> float:
        assert self.dim_rows == self.dim_cols, "Non-square matrix"
        ans = float(1)
        mat_copy = self.copy()
        for i in range(mat_copy.dim_rows):
            if dec_zero(mat_copy[i][i]):
                for j in range(i+1, mat_copy.dim_rows):
                    if not dec_zero(mat_copy[j][i]):
                        mat_copy.swap_rows(i, j)
                        mat_copy.row_scale(j, float(-1))
                        break
            if dec_zero(mat_copy[i][i]):
                return float(0)
            ans *= mat_copy[i][i]
            mat_copy.row_scale(i, float(1)/mat_copy[i][i])
            for j in range(i + 1, mat_copy.dim_rows):
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
        return ans

    def inv(self) -> Matrix:
        assert self.dim_rows == self.dim_cols, "Non-square matrix"
        ans = Matrix([[int(i == j) for j in range(self.dim_rows)] for i in range(self.dim_rows)])
        mat_copy = self.copy()

        # Прямой ход
        for i in range(mat_copy.dim_rows):
            # Ищем строку с ненулевым элементом на i-ой позиции
            if dec_zero(mat_copy[i][i]):
                for j in range(i + 1, mat_copy.dim_rows):
                    if not dec_zero(mat_copy[j][i]):
                        mat_copy.swap_rows(i, j)
                        ans.swap_rows(i, j)
                        break

            # Убеждаемся в том, что нашли строку с ненулевым элементом
            assert not dec_zero(mat_copy[i][i]), 'Zero-determinant matrix A'

            # Операция прямого хода
            ans.row_scale(i, float(1) / mat_copy[i][i])
            mat_copy.row_scale(i, float(1) / mat_copy[i][i])
            for j in range(i + 1, mat_copy.dim_rows):
                ans.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])

        # Обратный ход
        for i in range(mat_copy.dim_rows - 1, -1, -1):
            for j in range(0, i):
                ans.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
        return ans

    def norm(self) -> float:
        ans = float(0)
        for j in range(self.dim_cols):
            cur = float(0)
            for i in range(self.dim_rows):
                cur += abs(self[i][j])
            ans = max(cur, ans)
        return ans


class LinearEquationWith1Sol(Matrix):
    def __init__(self,
                 matrix_a: Union[List[List[Union[
                     float,
                     int,
                     float
                 ]]], Matrix],
                 matrix_f: Union[List[List[Union[
                     float,
                     int,
                     float
                 ]]], Matrix]) -> NoReturn:
        super().__init__(matrix_a)
        self.dim = self.dim_rows
        assert self.dim == self.dim_cols, "Non-square matrix A"
        self.f = Matrix(matrix_f)
        assert self.f.dim_cols == 1, "Entered F is not vector-column"
        assert self.f.dim_rows == self.dim, "Incorrect number of elements in F vector"
        self.align = Matrix([[i for i in range(self.dim)]])

    def __copy__(self) -> Matrix:
        return LinearEquationWith1Sol(self.matrix, self.f.matrix)

    def print(self, dec_places: int = 5) -> NoReturn:
        for i in range(self.dim_rows):
            first = i == 0
            last = (i + 1) == self.dim_rows
            if first and last:
                print('( ', end='')
            elif first:
                print('/ ', end='')
            elif last:
                print('\\ ', end='')
            else:
                print('| ', end='')
            for item in self[i]:
                print(round(item, dec_places), end=' ')
            print(f"| {round(self.f[i][0], dec_places)}", end=' ')
            if first and last:
                print(' )')
            elif first:
                print(' \\')
            elif last:
                print(' /')
            else:
                print(' |')

    def tex(self, dec_places: int = 2, max_row_cnt: int = 10, max_col_cnt: int = 10) -> str:
        ans = '\\left[\\begin{tabular}{ '
        for i in range(min(self.dim_cols, max_col_cnt + 2)):
            ans += 'c '
        ans += '| c }\n'
        for i in range(min(self.dim_rows, max_row_cnt + 1)):
            if (self.dim_rows <= max_row_cnt) or (i < max_row_cnt):
                ans += ' & '.join(
                    [str(round(self.matrix[i][j], dec_places))
                     if (self.dim_cols <= max_col_cnt) or (j < max_col_cnt) else
                     ('\\cdots' if i == min(self.dim_rows, max_row_cnt + 1) // 2 else '')
                     for j in range(min(self.dim_cols, max_col_cnt + 1))])
                if self.dim_cols > max_col_cnt:
                    ans += ' & ' + str(
                        round(self.matrix[i][-1], dec_places))
                ans += ' & ' + str(
                    round(self.f[i][0], dec_places))
            elif i == max_row_cnt:
                ans += ' & '.join(['\\vdots' if j == min(self.dim_cols, max_col_cnt + 1) // 2 else ('\\ddots' if i == j else '')
                                   for j in range(min(self.dim_cols, max_col_cnt + 1))])
                ans += ' &  \\\\\n'
                ans += ' & '.join(
                    [str(round(self.matrix[-1][j], dec_places))
                     if (self.dim_cols <= max_col_cnt) or (j < max_col_cnt) else
                     ('\\cdots' if i == min(self.dim_rows, max_row_cnt + 1) // 2 else '')
                     for j in range(min(self.dim_cols, max_col_cnt + 1))])
                ans += ' & ' + str(
                    round(self.matrix[-1][-1], dec_places))
                ans += ' & ' + str(
                    round(self.f[-1][0], dec_places))
            if i != min(self.dim_rows, max_row_cnt + 1) - 1:
                ans += ' \\\\\n'
        ans += '\n\\end{tabular}\\right]'
        return ans

    def restore_arrangement(self) -> NoReturn:
        for i in range(self.dim):
            while int(self.align[0][i]) != i:
                self.swap_cols(i, int(self.align[0][i]))
        for i in range(self.dim):
            while not dec_one(self[i][i]):
                for j in range(self.dim):
                    if not dec_zero(self[i][j]):
                        self.swap_rows(i, j)
                        break

    def swap_cols(self, col1: int, col2: int) -> NoReturn:
        super().swap_cols(col1, col2)
        self.align.swap_cols(col1, col2)

    def swap_rows(self, row1: int, row2: int) -> NoReturn:
        super().swap_rows(row1, row2)
        self.f.swap_rows(row1, row2)

    def row_scale(self,
                  row: int,
                  scale: Union[int, float, float]) -> NoReturn:
        super().row_scale(row, scale)
        self.f.row_scale(row, scale)

    def col_scale(self,
                  col: int,
                  scale: Union[int, float, float]) -> NoReturn:
        assert False, 'Column modification not allowed'

    def row_linear_operation(self,
                             row1: int,
                             row2: int,
                             scale: Union[int, float, float]) -> NoReturn:
        super().row_linear_operation(row1, row2, scale)
        self.f.row_linear_operation(row1, row2, scale)

    def column_linear_operation(self,
                                col1: int,
                                col2: int,
                                scale: Union[int, float, float]) -> NoReturn:
        assert False, 'Column operations not allowed'

    def solve(self) -> Matrix:
        return self.solve_gauss_classic()

    def solve_gauss_classic(self) -> Matrix:
        mat_copy = self.copy()

        # Прямой ход
        for i in range(mat_copy.dim):
            # Ищем строку с ненулевым элементом на i-ой позиции
            if dec_zero(mat_copy[i][i]):
                for j in range(i+1, mat_copy.dim):
                    if not dec_zero(mat_copy[j][i]):
                        mat_copy.swap_rows(i, j)
                        break

            # Убеждаемся в том, что нашли строку с ненулевым элементом
            assert not dec_zero(mat_copy[i][i]), 'Zero-determinant matrix A'

            # Операция прямого хода
            mat_copy.row_scale(i, float(1)/mat_copy[i][i])
            for j in range(i + 1, mat_copy.dim):
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])

        # Обратный ход
        for i in range(mat_copy.dim-1, -1, -1):
            for j in range(0, i):
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
        return mat_copy.f

    def solve_gauss_main_element(self) -> Matrix:
        mat_copy = self.copy()

        # Прямой ход
        for i in range(mat_copy.dim):
            # Выбор главного элемента
            mx_ind = i
            for j in range(i+1, mat_copy.dim):
                if mat_copy[i][j] > mat_copy[i][mx_ind]:
                    mx_ind = j
            mat_copy.swap_cols(i, mx_ind)

            # Убеждаемся в том, что нашли ненулевой элемент
            assert not dec_zero(mat_copy[i][i]), 'Zero-determinant matrix A'

            # Выполняем прямой ход
            mat_copy.row_scale(i, float(1) / mat_copy[i][i])
            for j in range(i + 1, mat_copy.dim):
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])

        # Обратный ход
        for i in range(mat_copy.dim-1, -1, -1):
            for j in range(0, i):
                mat_copy.row_linear_operation(i, j, float(-1) * mat_copy[j][i])
        mat_copy.restore_arrangement()
        return mat_copy.f

    def solve_over_relaxation(self, omega: float, eps_residual: float) -> (Matrix, int):
        cur_x = zero_matrix(self.dim, 1)
        flag = True
        cnt = 0
        while (self * cur_x - self.f).norm() > eps_residual:
            new_x = zero_matrix(self.dim, 1)
            for i in range(self.dim):
                f_1 = self.f[i][0]
                for j in range(i):
                    f_1 -= self[i][j] * new_x[j][0]
                for j in range(i,self.dim):
                    f_1 -= self[i][j] * cur_x[j][0]
                new_x[i][0] = cur_x[i][0] + (omega/self[i][i])*f_1
            cur_x = new_x.copy()
            cnt += 1
        return cur_x, cnt




def vector_row(vector: List[Union[
                   float,
                   int,
                   float
               ]]) -> Matrix:
    return Matrix([vector])


def vector_col(vector: List[Union[
                   float,
                   int,
                   float
               ]]) -> Matrix:
    return vector_row(vector).transposed()


def func_matrix(dim_rows: int,
                dim_cols: int,
                func: Callable[[int, int, int, int], float]) -> Matrix:
    return Matrix([[func(i + 1, j + 1, dim_rows, dim_cols) for j in range(dim_cols)] for i in range(dim_rows)])


def zero_matrix(dim_rows: int,
                dim_cols: int) -> Matrix:
    return func_matrix(dim_rows, dim_cols, (lambda row, col, dim_r, dim_c: 0))
