from lib.linear_system_helper import LinearEquationWith1Sol, vector_col, func_matrix, Matrix, CNT
from typing import *
from math import *
import sys
import json
from functools import wraps
import errno
import os
import signal

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator


@timeout(2)
def solve_with_timeout(sys, *args, **kwargs) -> int:
    return sys.solve_over_relaxation(*args, **kwargs)[1]


class InputData(TypedDict):
    type: str
    n: int
    dec_places: int
    max_rows: int
    max_cols: int
    iter: bool
    eps_residual: float
    tex: bool
    omega: float
    func_a: str
    func_f: str
    x: float
    matrix_a: List[List[Union[float, int]]]
    vector_f: List[Union[float, int]]


with open(sys.argv[1], 'r') as input_file:
    input_data: InputData = json.load(input_file)


if 'dec_places' not in input_data:
    input_data['dec_places'] = 2
if 'max_rows' not in input_data:
    input_data['max_rows'] = 10
if 'max_cols' not in input_data:
    input_data['max_cols'] = 10
if 'iter' not in input_data:
    input_data['iter'] = False
if 'omega' not in input_data:
    input_data['omega'] = 1
if 'x' not in input_data:
    input_data['x'] = 1
if 'tex' not in input_data:
    input_data['tex'] = False
if 'eps_residual' not in input_data:
    input_data['eps_residual'] = 0.01

matrix: Matrix
vector_f: Matrix

if input_data['type'] == 'function':
    def func_a(i: int, j: int, n: int, n1: int) -> float:
        x = input_data['x']
        return eval(input_data['func_a'])

    def func_f(i: int, j: int, n: int, n1: int) -> float:
        x = input_data['x']
        return eval(input_data['func_f'])

    matrix = func_matrix(dim_rows=input_data['n'],
                         dim_cols=input_data['n'],
                         func=func_a)

    vector_f = func_matrix(dim_rows=input_data['n'],
                           dim_cols=1,
                           func=func_f)
elif input_data['type'] == 'matrix':
    matrix = Matrix(input_data['matrix_a'])
    vector_f = Matrix(vector_col(input_data['vector_f']))
else:
    assert False, "Incorrect input"


system = LinearEquationWith1Sol(matrix_a=matrix,
                                matrix_f=vector_f)

print("Input:")
if input_data['tex']:
    print(system.tex(dec_places=input_data['dec_places'], max_row_cnt=input_data['max_rows'], max_col_cnt=input_data['max_cols']))
else:
    system.print()
print(f"Число обусловенности: {system.norm() * system.inv().norm()}")
if input_data['iter']:
    sol = system.solve_over_relaxation(input_data['omega'], input_data['eps_residual'])[0]
    print(f"Решение методом верхней релаксации с параметром равным {str(input_data['omega'])}:")
    if input_data['tex']:
        print(sol.tex(dec_places=input_data['dec_places'], max_row_cnt=input_data['max_rows'],
                      max_col_cnt=input_data['max_cols']))
    else:
        sol.print()
    cur = 0.2
    for i in range(9):
        print(f"{round(cur,2)}: ", end="")
        try:
            print(solve_with_timeout(system, cur, input_data['eps_residual']))
        except:
            print("не сходится или слишком много операций")
        cur += 0.2
else:
    sol1 = system.solve_gauss_classic()
    sol2 = system.solve_gauss_main_element()
    if input_data['tex']:
        print("Решение классическим алгоритмом Гаусса:")
        print(sol1.tex(dec_places=input_data['dec_places'], max_row_cnt=input_data['max_rows'], max_col_cnt=input_data['max_cols']))
        print("Решение алгоритмом Гаусса с выбором главного элемента:")
        print(sol2.tex(dec_places=input_data['dec_places'], max_row_cnt=input_data['max_rows'], max_col_cnt=input_data['max_cols']))
    else:
        print("Решение классическим алгоритмом Гаусса:")
        sol1.print(dec_places=input_data['dec_places'])
        print("Решение алгоритмом Гаусса с выбором главного элемента:")
        sol2.print(dec_places=input_data['dec_places'])
